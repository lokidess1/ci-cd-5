import os
import json
import requests


def create_output():

    url = "http://api.open-notify.org/astros.json"
    response = requests.get(url)

    if not os.path.exists('results'):
        os.mkdir('results')

    if response.status_code == 200:
        with open('results/astronauts.json', 'w') as result:
            json.dump(response.json(), result, indent=4)
        print(response.text)


if __name__ == '__main__':
    create_output()
