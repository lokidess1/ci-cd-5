from app import create_output
from unittest.mock import patch


@patch('app.requests.get')
def test_create_output(get_mock):
    expected = "http://api.open-notify.org/astros.json"
    create_output()
    get_mock.assert_called_once_with(expected)


@patch('app.os.mkdir')
@patch('app.os.path.exists', return_value=True)
@patch('app.requests.get')
def test_create_output_results_not_exists(get_mock, exists_mock, mkdir_mock):
    create_output()
    assert mkdir_mock.call_count == 0


@patch('app.os.mkdir')
@patch('app.os.path.exists', return_value=False)
@patch('app.requests.get')
def test_create_output_results_exists(get_mock, exists_mock, mkdir_mock):
    create_output()
    mkdir_mock.assert_called_once()
